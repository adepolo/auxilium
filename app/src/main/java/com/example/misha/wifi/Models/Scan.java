package com.example.misha.wifi.Models;

/**
 * The Scan model represents the result of the access point scan performed by the application.
 *
 * @author Michael Adeyinfa, Jason Ngo
 * @version 4.0
 * @since 2018-04-20
 * */
public class Scan {
    // The address of the access point.
    private String bssid ;
    // The network name.
    private String ssid ;
    // Describes the authentication, key management, and encryption schemes supported by the access point.
    private String capabilities ;
    // Not used if the AP bandwidth is 20 MHz If the AP use 40, 80 or 160 MHz, this is the center frequency (in MHz)
    // if the AP use 80 + 80 MHz, this is the center frequency of the first segment (in MHz)
    private int centerFreq0 ;
    // Only used if the AP bandwidth is 80 + 80 MHz if the AP use 80 + 80 MHz,
    // this is the center frequency of the second segment (in MHz)
    private int centerFreq1 ;
    // AP Channel bandwidth; one of CHANNEL_WIDTH_20MHZ, CHANNEL_WIDTH_40MHZ,
    // CHANNEL_WIDTH_80MHZ, CHANNEL_WIDTH_160MHZ or CHANNEL_WIDTH_80MHZ_PLUS_MHZ.
    private int chanelWidth ;
    // The primary 20 MHz frequency (in MHz) of the channel over which the client is communicating with the access point.
    private int frequency ;
    // The detected signal level in dBm, also known as the RSSI.
    private int level ;

    // timestamp in microseconds (since boot) when this result was last seen.
    private long timestamp;

    public String getBssid() {
        return bssid;
    }

    public void setBssid(String bssid) {
        this.bssid = bssid;
    }

    public String getSsid() {
        return ssid;
    }

    public void setSsid(String ssid) {
        this.ssid = ssid;
    }

    public String getCapabilities() {
        return capabilities;
    }

    public void setCapabilities(String capabilities) {
        this.capabilities = capabilities;
    }

    public int getCenterFreq0() {
        return centerFreq0;
    }

    public void setCenterFreq0(int centerFreq0) {
        this.centerFreq0 = centerFreq0;
    }

    public int getCenterFreq1() {
        return centerFreq1;
    }

    public void setCenterFreq1(int centerFreq1) {
        this.centerFreq1 = centerFreq1;
    }

    public int getChanelWidth() {
        return chanelWidth;
    }

    public void setChanelWidth(int chanelWidth) {
        this.chanelWidth = chanelWidth;
    }

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    // Public constructor using fields.
    public Scan(String bssid, String ssid, String capabilities, int centerFreq0, int centerFreq1, int chanelWidth, int frequency, int level, long timestamp) {
        setBssid(bssid);
        setSsid(ssid);
        setCapabilities(capabilities);
        setCenterFreq0(centerFreq0);
        setCenterFreq1(centerFreq1);
        setChanelWidth(chanelWidth);
        setFrequency(frequency);
        setLevel(level);
        setTimestamp(timestamp);
    }


}
