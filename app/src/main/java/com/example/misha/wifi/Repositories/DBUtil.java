package com.example.misha.wifi.Repositories;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.util.Log;
import android.widget.TextView;

import com.example.misha.wifi.Models.Scan;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

/**
 * This class was created to handle the Asynchronous requests to the remote server for more
 * calculations.
 */
public class DBUtil {

    public Context context;

    public final String TAG = getClass().toString();


    public void requestLocation(){

        try{
            RequestParams params = new RequestParams();
            params.put("Latitude","37.418436" );
            params.put("Longitude", "-121.963477");

            AsyncHttpClient client = new AsyncHttpClient();
            client.get("http://adepolo.us-west-2.elasticbeanstalk.com/tril/elevation/", params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    // handle your response from the server here
                    String b =  new String(responseBody);
                    Log.d(TAG,"Success " + b);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    Log.e(TAG,"Error " + new String(responseBody));
                }

            });
        }catch(Exception ex){
            Log.d(TAG, ex.getMessage());
        }
    }

    /**
     *  Specific method created to handle the sending of the list of Access Points to the remote
     *  server.
     * @param apList Filtered List of ScanResults from AccessPointsScanRepo.
     */
    public void sendAccessPointsToServer (List<ScanResult> apList) {

        Gson gson = new Gson();
        List<Scan> scanList = new ArrayList<Scan>();

        // add each object in apList to the list of scan objects
        for(ScanResult g : apList){
            scanList.add(new Scan(g.BSSID, g.SSID, g.capabilities, g.centerFreq0, g.centerFreq0, g.channelWidth, g.frequency, g.level,  g.timestamp));
        }


        Type listType = new TypeToken<ArrayList<Scan>>() {}.getType();
        String json = gson.toJson(scanList,  listType);
        RequestParams params = new RequestParams();
        params.put("location123", json);
        AsyncHttpClient client =  null;
        try {
            //JSONObject jsonParams = new JSONObject();
            //jsonParams.put("results", "Test api support");
            StringEntity entity = new StringEntity(json);

            client = new AsyncHttpClient();
            client.get( context ,"http://adepolo.us-west-2.elasticbeanstalk.com/tril/", entity,"application/json" ,new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    // handle your response from the server here
                    String b =  new String(responseBody);
                    Log.d(TAG, "Success " + b);
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    String b =  new String(responseBody);
                    Log.e(TAG, "Error " + b);
                }

            });
            Log.d("Success", client.toString());
        } catch (Exception ex) {
            Log.d(TAG, ex.getMessage().toString() + "");
        }

    }
}


