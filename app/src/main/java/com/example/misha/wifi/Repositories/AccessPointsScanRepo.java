package com.example.misha.wifi.Repositories;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import java.util.ArrayList;
import java.util.List;

/**
    Class created to handle the initiation of Access Point Scanning.
 */
public class AccessPointsScanRepo {

    /** Declaring the global variables to be used in the repository. */
	WifiManager wifiScanner;
	List<ScanResult> listOfScannedAccessPoints;
	List<ScanResult> filteredAccessPointsBySchoolSSID = new ArrayList<>();
	private DBUtil databaseUtility = new DBUtil();
	Context context;

    /**
     * This method is to initiate the scanning for the access points.
     */
	public void startScanning() {
		try {
		    // initialize the WifiManager
			wifiScanner = (WifiManager) context.getApplicationContext()
                    .getSystemService(Context.WIFI_SERVICE);
			/* Register a BroadcastReceiver to be run in the main activity thread.
			 The receiver will be called with any broadcast Intent that matches filter,
			  in the main application thread.
			 */
			context.getApplicationContext().registerReceiver(mWifiScanReceiver,
					new IntentFilter(wifiScanner.SCAN_RESULTS_AVAILABLE_ACTION));
			wifiScanner.setWifiEnabled(true);
			wifiScanner.startScan();

		} catch (RuntimeException ex) {
		    // Else catch the runtimeException and print to the Stack Trace
            ex.printStackTrace();
		}
	}

    /**
     * This is an Android component which allows you to register for system or application
     * events. All registered receivers for an event are notified by the Android runtime
     * once this event happens.
     */
	private final BroadcastReceiver mWifiScanReceiver = new BroadcastReceiver() {
	    /* Denotes that the annotated element should only be called on the given API
	     level or higher. */
		@RequiresApi(api = Build.VERSION_CODES.N)

        /**
        * If the event for which the broadcast receiver has registered happens,
         * the onReceive() method of the receiver is called by the Android system.
         * @param c Context on which the receiver was registered.
         * @param intent The intent being received.
         * */
		@Override
		public void onReceive(Context c, Intent intent) {
			try {
			    // When sending a broadcast the recipient is allowed
                // to run at foreground priority, with a shorter timeout interval.
				intent.addFlags(Intent.FLAG_RECEIVER_FOREGROUND);
                // An access point scan has completed, results are available and its matches the
                // action to be performed by the Intent
				if (intent.getAction().equals(wifiScanner.SCAN_RESULTS_AVAILABLE_ACTION)) {
				    // Initialize the list of scan results
					listOfScannedAccessPoints = wifiScanner.getScanResults();
					// For each scan result in the list, if the Network Name is
                    // "Sheridan Secure Access", first check to ensure that the list size is less
                    // than 3, if not, then add to the filtered list of access points.
					for (ScanResult accessPoint : listOfScannedAccessPoints) {
						if (accessPoint.SSID.equals("Sheridan Secure Access")) {
							if(filteredAccessPointsBySchoolSSID.size() > 3){
								break;
							}
							filteredAccessPointsBySchoolSSID.add(accessPoint);
						}
					}
                    // Send the filtered list of access points to the remote server for the required
                    // calculations.
					databaseUtility.sendAccessPointsToServer(filteredAccessPointsBySchoolSSID);

				}
			} catch (RuntimeException ex) {
                // Else catch the runtimeException and print to the Stack Trace
				ex.printStackTrace();

			}
		}
	};
}
