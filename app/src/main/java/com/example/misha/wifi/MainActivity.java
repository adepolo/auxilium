package com.example.misha.wifi;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.StrictMode;

import android.support.annotation.RequiresApi;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.LocalBroadcastManager;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.misha.wifi.Repositories.AccessPointsScanRepo;



/*
    MainActivity Location Tracking, locates the nearest Access Point to your Android Device's location
    (In this case, device used was a OnePlus3T). Displays AP MAC address and estimated distance away.
    LOCATION MUST BE ENABLED IN ORDER FOR DEVICE TO WORK.
     Button may be used for further functionality later on.
* */
public class MainActivity extends Activity {
    
    BroadcastReceiver receiver;
    Button activateLocationTrackingButton;
    ProgressBar searchProgress;
    TextView currentLocation;
    TextView firstScannedAccessPoint, secondScannedAccessPoint, thirdScannedAccessPoint;
    TextInputLayout to;
	AccessPointsScanRepo scan = new AccessPointsScanRepo();

    @TargetApi(21)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        permitAllWebsiteCertifications();

        inflateAndSetupGUI();
        searchProgress.setVisibility(View.GONE);

        activateLocationTrackingButton.setOnClickListener(v -> {
            Animation fadeOut = new AlphaAnimation(1, 0);
            fadeOut.setInterpolator(new AccelerateInterpolator());
            fadeOut.setDuration(1000);

            fadeOut.setAnimationListener(new Animation.AnimationListener()
            {
                public void onAnimationEnd(Animation animation)
                {
                    to.setVisibility(View.GONE);
                    activateLocationTrackingButton.setVisibility(View.GONE);

                    if(searchProgress.getVisibility() == View.VISIBLE) {
                        searchProgress.setVisibility(View.GONE);
                    }
                    else {
                        searchProgress.setVisibility(View.VISIBLE);
                    }
                }
                public void onAnimationRepeat(Animation animation) {}
                public void onAnimationStart(Animation animation) {}
            });

            to.startAnimation(fadeOut);
            activateLocationTrackingButton.startAnimation(fadeOut);

        });



        scan.startScanning();
    }

    @Override
    public void onResume() {
        super.onResume();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, 87);
            }
        }
    }

    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver(receiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
    }

    public void inflateAndSetupGUI() {
        currentLocation = findViewById(R.id.lblLocation);
        activateLocationTrackingButton = findViewById(R.id.search);
        firstScannedAccessPoint = findViewById(R.id.textView3);
        secondScannedAccessPoint = findViewById(R.id.t1);
        thirdScannedAccessPoint = findViewById(R.id.t2);
        searchProgress = findViewById(R.id.progressBar1);
        to = findViewById(R.id.input);
    }

    public void permitAllWebsiteCertifications() {
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
    }
    
}
